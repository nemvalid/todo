import React from 'react';
import { connect } from 'react-redux';
import { toggleTodo, deleteTodo, addTodo } from './actions';

const Todo = ({ onToggle, onDelete, todo }) => (
    <li className="todolist__item" >
        <label className="todolist__input">
            <input type="checkbox"
                checked={todo.done}
                onChange={() => onToggle(todo)} />

            <span style={{
                textDecoration: todo.done ? 'line-through' : 'none'
            }}>{todo.title}</span>
        </label>
        <button onClick={() => onDelete(todo.id)}>x</button>
    </li>
);

let App = ({ todos, isFetching, dispatch }) => {

    const onDeleteTodo = (id) => dispatch(deleteTodo(id));
    const onToggleTodo = (id) => dispatch(toggleTodo(id));
    const onAddTodo = (e) => {
        if (e.keyCode !== 13) return;
        if (!e.target.value) return;
        dispatch(addTodo(e.target.value));
        e.target.value = '';
    };

    return (
        <div>
            <center>{isFetching ? '⌛' : '✔'}</center>
            <input type="text"
                autoFocus
                onKeyUp={(e) => onAddTodo(e) } />

            <ul className="todolist">
                {todos.map(todo =>
                    <Todo
                        key={todo.id}
                        todo={todo}
                        onDelete={onDeleteTodo}
                        onToggle={onToggleTodo}
                    />
                )}
            </ul>
        </div>
    )
};

const mapStateToProps = (state) => {
    return {
        todos: state.todos,
        isFetching: state.isFetching
    }
}

App = connect(mapStateToProps)(App);

export default App;
