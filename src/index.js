import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from './reducers';
import App from './App';
import './index.css';
import { fetchTodos } from './actions';


const store = createStore(
    rootReducer, 
    applyMiddleware(
        thunkMiddleware
    )
);

store.dispatch(fetchTodos());

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
