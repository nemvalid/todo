import {
    ADD_TODO,
    ADD_TODO_COMPLETE,
    DELETE_TODO,
    DELETE_TODO_COMPLETE,
    TOGGLE_TODO,
    TOGGLE_TODO_COMPLETE,
    REQUEST_TODOS,
    RECEIVE_TODOS
} from './actions';

const rootReducer = (state = { todos: [], isFetching: false}, action) => {
    if (!action || !action.type) {
        return state;
    }
    switch (action.type) {
        case ADD_TODO:
            return Object.assign({}, state, {
                isFetching: true,
                todos: [...state.todos, action.todo]
            });
        case ADD_TODO_COMPLETE:
            return Object.assign({}, state, {isFetching: false});
        case DELETE_TODO:
            return Object.assign({}, state, { 
                todos: state.todos.filter(t => t.id !== action.id),
                isFetching: true 
            });
        case DELETE_TODO_COMPLETE:
            return Object.assign({}, state, {isFetching: false});
        case TOGGLE_TODO:
            return Object.assign({}, state, { 
                todos: state.todos.map(t => t.id === action.todo.id ? { ...t, done: !t.done } : t),
                isFetching: true 
            });
        case TOGGLE_TODO_COMPLETE:
            return Object.assign({}, state, {isFetching: false});
        case REQUEST_TODOS:
            return Object.assign({}, state, {isFetching: true});
        case RECEIVE_TODOS:
            return Object.assign({}, state, {isFetching: false, todos: action.todos});
        default:
            return state;
    }
};

export default rootReducer;