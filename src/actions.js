import uuid from 'uuid';

const ADD_TODO = 'ADD_TODO';
const ADD_TODO_COMPLETE = 'ADD_TODO_COMPLETE';
const DELETE_TODO = 'DELETE_TODO';
const DELETE_TODO_COMPLETE = 'DELETE_TODO_COMPLETE';
const TOGGLE_TODO = 'TOGGLE_TODO';
const TOGGLE_TODO_COMPLETE = 'TOGGLE_TODO_COMPLETE';
const REQUEST_TODOS = 'REQUEST_TODOS';
const RECEIVE_TODOS = 'RECEIVE_TODOS';

export { ADD_TODO, ADD_TODO_COMPLETE, DELETE_TODO, DELETE_TODO_COMPLETE, TOGGLE_TODO, TOGGLE_TODO_COMPLETE, RECEIVE_TODOS, REQUEST_TODOS };

const addTodoComplete = () => ({
    type: ADD_TODO_COMPLETE
});

const addTodo = (title) =>
    (dispatch) => {
        const todo = {
            id: uuid.v4(),
            title,
            done: false
        };
        dispatch({
            type: ADD_TODO,
            todo
        });

        fetch(`http://localhost:3004/todos`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(todo)
        }).then(response => {
            dispatch(addTodoComplete());
        });
    };

const toggleTodoComplete = () => ({
    type: TOGGLE_TODO_COMPLETE
});

const toggleTodo = (todo) => dispatch => {
    dispatch({
        type: TOGGLE_TODO,
        todo
    });
    
    fetch(`http://localhost:3004/todos/${todo.id}`, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(Object.assign({}, todo, { done: !todo.done}))
    }).then(response => {
        dispatch(toggleTodoComplete());
    });
};

const deleteTodoComplete = () => ({
    type: DELETE_TODO_COMPLETE
});

const deleteTodo = (id) => dispatch => {
    dispatch({
        type: DELETE_TODO,
        id
    });

    fetch(`http://localhost:3004/todos/${id}`, {
        method: 'DELETE'
    }).then(response => {
        dispatch(deleteTodoComplete());
    });
};

const requestTodos = () => ({
    type: REQUEST_TODOS
});
const receiveTodos = (todos) => ({
    type: RECEIVE_TODOS,
    todos
});

const fetchTodos = () =>
    (dispatch) => {
        dispatch(requestTodos());

        fetch('http://localhost:3004/todos')
            .then(response => response.json())
            .then(json => dispatch(receiveTodos(json)))
    };

export { 
    addTodo, 
    addTodoComplete, 
    toggleTodo, 
    toggleTodoComplete, 
    deleteTodo, 
    deleteTodoComplete, 
    fetchTodos 
};